from pages.base_page import BasePage


class GroupsListPage(BasePage):
    GROUP_LOCATOR = "//a[text()='{}']"  # //a[text()='test_group_2']

    def is_group_present(self, group):
        return self.is_element_present(self.GROUP_LOCATOR.format(group))



