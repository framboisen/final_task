from pages.base_page import BasePage


class LoginPage(BasePage):
    USERNAME_INPUT_LOCATOR = "//input[@name='username']"
    PASSWORD_INPUT_LOCATOR = "//input[@name='password']"
    LOGIN_BUTTON_LOCATOR = "//input[@type='submit']"

    def login(self, username, password):
        self.enter_text(self.USERNAME_INPUT_LOCATOR, username)
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)
        self.click(self.LOGIN_BUTTON_LOCATOR)

