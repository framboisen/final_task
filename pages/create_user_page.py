from pages.base_page import BasePage


class CreateUserPage(BasePage):
    ADD_USERNAME_LOCATOR = "//input[@name='username']"
    ADD_PASSWORD_LOCATOR = "//input[@name='password1']"
    CONFIRM_PASSWORD_LOCATOR = "//input[@name='password2']"
    SAVE_USER_LOCATOR = "//input[@value='Save']"

    def create_new_user(self, username, password):
        self.enter_text(self.ADD_USERNAME_LOCATOR, username)
        self.enter_text(self.ADD_PASSWORD_LOCATOR, password)
        self.enter_text(self.CONFIRM_PASSWORD_LOCATOR, password)
        self.click(self.SAVE_USER_LOCATOR)


