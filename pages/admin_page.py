from pages.base_page import BasePage


class AdminPage(BasePage):
    ADD_USER_BUTTON_LOCATOR = "//a[@href='/admin/auth/user/add/']"
    ADD_GROUP_BUTTON_LOCATOR = "//a[@href='/admin/auth/group/add/']"

    USERS_PAGE_LOCATOR = "//a[@href='/admin/auth/user/']"
    GROUPS_PAGE_LOCATOR = "//a[@href='/admin/auth/group/']"

    def proceed_to_add_user(self):
        self.click(self.ADD_USER_BUTTON_LOCATOR)

    def proceed_to_add_group(self):
        self.click(self.ADD_GROUP_BUTTON_LOCATOR)

    def open_user_list(self):
        self.click(self.USERS_PAGE_LOCATOR)

    def open_group_list(self):
        self.click(self.GROUPS_PAGE_LOCATOR)



