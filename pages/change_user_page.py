from pages.base_page import BasePage


class ChangeUserPage(BasePage):
    CHOOSE_GROUP_LOCATOR = "//option[@title='{}']"
    ADD_GROUP_TO_LIST_LOCATOR = "//a[@id='id_groups_add_link']"
    SAVE_CHANGES_LOCATOR = "//input[@value='Save']"

    def add_user_to_group(self, group):
        self.click(self.CHOOSE_GROUP_LOCATOR.format(group))
        self.click(self.ADD_GROUP_TO_LIST_LOCATOR)
        self.click(self.SAVE_CHANGES_LOCATOR)



