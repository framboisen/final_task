from pages.base_page import BasePage


class MainPage(BasePage):
    URL = 'http://localhost:8000/'
    ENTER_IN_ADMIN_LOCATOR = "//a[@href='/admin']"

    def proceed_to_login(self):
        self.click(self.ENTER_IN_ADMIN_LOCATOR)
