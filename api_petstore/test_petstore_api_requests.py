import json
import requests
import pytest


class TestApiRequests:
    def test_create_user(self):
        url = "https://petstore.swagger.io/v2/user"
        data = {
            "id": 1,
            "username": "nastya_qap04",
            "firstName": "nastya",
            "lastName": "feshchenko",
            "email": "feshchenko.nastya@gmail.com",
            "password": "nastyaspassword",
            "phone": "1234567",
            "userStatus": 0}

        response = requests.post(url, json=data)
        create_user_data = response.json
        assert response.status_code == 200, "Your attempt to create a user failed"

    def test_login(self):
        url = "https://petstore.swagger.io/v2/user/login"
        params = {"username": "nastya_qap04",
                  "password": "nastyaspassword"}

        response = requests.get(url, params=params)
        login_data = response.json
        assert response.status_code == 200, "You are NOT logged in"

    def test_get_user_data(self):
        url = "https://petstore.swagger.io/v2/user/nastya_qap04"
        response = requests.get(url)
        user_data = response.json
        string_user_data = json.loads(response.text)
        assert response.status_code == 200, "Failed to get user's data"

    def test_logout(self):
        url = "https://petstore.swagger.io/v2/user/logout"
        response = requests.get(url)
        logout_data = response.json()
        string_logout_data = json.loads(response.text)
        assert response.status_code == 200, "Failed to log out"

    def test_delete_user(self):
        url = "https://petstore.swagger.io/v2/user/nastya_qap04"
        response = requests.delete(url)
        delete_user_data = response.json()
        string_delete_user_data = json.loads(response.text)
        assert response.status_code == 200, "User has NOT been deleted"