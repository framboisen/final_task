import pytest
import allure


@pytest.mark.usefixtures("login")
class TestGroups:
    @allure.description("The test is needed to check the process of creating a group")
    def test_create_a_group(self, database, admin_page, groups_list_page):
        with allure.step("Creating a new group in the database"):
            database.create_a_group_in_database("1", "test_group_1")
        with allure.step("Checking if the group has been created"):
            admin_page.open_group_list()
            check_group_presence = groups_list_page.is_group_present("test_group_1")

        assert check_group_presence, "Group has NOT been created"

    @allure.description("The test is needed to check the process of adding a user to a group")
    def test_add_user_to_group(self, admin_page, create_user_page, change_user_page, database):
        with allure.step("Creating a user on the website"):
            admin_page.proceed_to_add_user()
            create_user_page.create_new_user("test_user_1", "not_a_common_password!")
            change_user_page.add_user_to_group("test_group_1")
        with allure.step("Getting the user's and the group's ids from the database"):
            user_id = database.get_user_id("test_user_1")
            group_id = database.get_group_id("test_group_1")
            user_group_id = database.get_user_group_id(user_id)

        assert group_id == user_group_id, "User has NOT been added to the group"




