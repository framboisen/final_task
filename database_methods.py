import psycopg2

connection = psycopg2.connect(dbname='postgres', user='postgres', password='postgres', host='localhost')
cursor = connection.cursor()


class Database:
    def create_a_group_in_database(self, id, name):
        query = "INSERT INTO auth_group (id, name) VALUES (%s, %s)"
        values = (f"{format(id)}", f"{format(name)}")
        cursor.execute(query, values)
        connection.commit()

    def get_user_id(self, username):
        cursor.execute(f"SELECT id FROM auth_user WHERE username = '{format(username)}'")
        database_user_id = cursor.fetchall()
        user_id = database_user_id[0][0]
        return user_id

    def get_group_id(self, group):
        cursor.execute(f"SELECT id FROM auth_group WHERE name = '{format(group)}'")
        database_group_id = cursor.fetchall()
        group_id = database_group_id[0][0]
        return group_id

    def get_user_group_id(self, user_id):
        cursor.execute(f"SELECT group_id FROM auth_user_groups WHERE user_id = '{format(user_id)}'")
        database_user_group_id = cursor.fetchall()
        user_group_id = database_user_group_id[0][0]
        return user_group_id









