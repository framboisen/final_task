import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from pages.main_page import MainPage
from pages.login_page import LoginPage
from pages.admin_page import AdminPage
from pages.create_user_page import CreateUserPage
from pages.change_user_page import ChangeUserPage
from pages.groups_page import GroupsListPage
from database_methods import Database

url = 'http://localhost:8000/'


@pytest.fixture(autouse=True)
def navigate_to_main_page_and_login(chromedriver):
    main_page = MainPage(chromedriver)
    main_page.navigate(url)
    main_page.proceed_to_login()
    yield
    chromedriver.quit()


@pytest.fixture()
def database():
    return Database()


@pytest.fixture()
def chromedriver():
    return webdriver.Chrome(ChromeDriverManager().install())


@pytest.fixture()
def main_page(chromedriver):
    return MainPage(chromedriver)


@pytest.fixture()
def proceed_to_login(chromedriver):
    return main_page.proceed_to_login()


@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
def login(login_page):
    return login_page.login("admin", "password")


@pytest.fixture()
def admin_page(chromedriver):
    return AdminPage(chromedriver)


@pytest.fixture()
def groups_list_page(chromedriver):
    return GroupsListPage(chromedriver)


@pytest.fixture()
def create_user_page(chromedriver):
    return CreateUserPage(chromedriver)


@pytest.fixture()
def change_user_page(chromedriver):
    return ChangeUserPage(chromedriver)











